package ic.com.calculator.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import ic.com.calculator.R;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME    = "calculator.db";
    private static final int    DATABASE_VERSION = 1;

    // the DAO object we use to access the SimpleData table
    private Dao<NumberData, Integer>                 numberDao        = null;
    private RuntimeExceptionDao<NumberData, Integer> numberRuntimeDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, NumberData.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, NumberData.class, true);
            // after we drop the old databases, we create the new ones
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the Database Access Object (DAO) for our NumberData class. It will create it or just
     * give the cached value.
     */
    public Dao<NumberData, Integer> getDao() throws SQLException {
        if (numberDao == null) {
            numberDao = getDao(NumberData.class);
        }
        return numberDao;
    }

    /**
     * Returns the RuntimeExceptionDao (Database Access Object) version of a Dao for our NumberData class. It will
     * create it or just give the cached value. RuntimeExceptionDao only through RuntimeExceptions.
     */
    public RuntimeExceptionDao<NumberData, Integer> getNumberDataDao() {
        if (numberRuntimeDao == null) {
            numberRuntimeDao = getRuntimeExceptionDao(NumberData.class);
        }
        return numberRuntimeDao;
    }

    @Override
    public void close() {
        super.close();
        numberDao = null;
        numberRuntimeDao = null;
    }
}
