package ic.com.calculator.database;

import com.j256.ormlite.field.DatabaseField;

public class NumberData {

    @DatabaseField(generatedId = true)
    int    id;
    @DatabaseField(canBeNull = false, unique = true)
    double number;
    @DatabaseField(canBeNull = false)
    double processedvalue;

    NumberData() {
        // needed by ormlite
    }

    public NumberData(double number, double value) {
        this.number = number;
        this.processedvalue = value;
    }

    public int getId() {
        return id;
    }

    public double getNumber() {
        return number;
    }

    public double getProcessedvalue() {
        return processedvalue;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("id=").append(id);
        sb.append(", ").append("number=").append(number);
        sb.append(", ").append("processedValue=").append(processedvalue);
        return sb.toString();
    }
}
