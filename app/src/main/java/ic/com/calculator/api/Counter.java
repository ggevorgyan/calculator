package ic.com.calculator.api;

import ic.com.calculator.api.model.MyNumberProto;
import ic.com.calculator.api.model.MyResponseProto;

public interface Counter {
    MyResponseProto.MyResponse count(MyNumberProto.MyNumber myNumber);
}
