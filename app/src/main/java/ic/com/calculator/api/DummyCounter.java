package ic.com.calculator.api;

import android.util.Log;

import ic.com.calculator.api.model.MyNumberProto;
import ic.com.calculator.api.model.MyResponseProto;

/**
 * Dummy class to imitate requests to server
 */
public class DummyCounter implements Counter {

    private static final String TAG = DummyCounter.class.getName();

    public MyResponseProto.MyResponse count(MyNumberProto.MyNumber myNumber) {
        MyResponseProto.MyResponse.Builder response = MyResponseProto.MyResponse.newBuilder()
                .setNumber(myNumber.getNumber());
        try {
            Thread.sleep(10000);
            response.setDivided(myNumber.getNumber() / 2);
        } catch (InterruptedException e) {
            Log.e(TAG, e.getMessage());
            response.setWithError(true);
        }
        return response.build();
    }
}
