package ic.com.calculator.api;

import ic.com.calculator.api.model.MyNumberProto;
import ic.com.calculator.api.model.MyResponseProto;

public class Calculator {
    private Counter mCounter;

    public Calculator(Counter counter) {
        mCounter = counter;
    }

    public MyResponseProto.MyResponse calculate(MyNumberProto.MyNumber myNumber) {
        return mCounter.count(myNumber);
    }
}
