package ic.com.calculator.api;

import android.util.Log;

import ic.com.calculator.api.model.MyNumberProto;
import ic.com.calculator.api.model.MyResponseProto;

public class DummyCounterWithError implements Counter {

    private static final String TAG = DummyCounterWithError.class.getName();

    public MyResponseProto.MyResponse count(MyNumberProto.MyNumber myNumber) {
        MyResponseProto.MyResponse.Builder response = MyResponseProto.MyResponse.newBuilder()
                .setNumber(myNumber.getNumber());
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            Log.e(TAG, e.getMessage());
        }
        response.setWithError(true);
        return response.build();
    }
}
