package ic.com.calculator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import ic.com.calculator.service.BroadcastHelper;
import ic.com.calculator.service.CalcService;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String STATE_RESULT              = "MainActivity.STATE.result";
    private static final String STATE_PROGRESS_VISIBILITY = "MainActivity.STATE.progress_visibility";

    private AutoCompleteTextView mNumberInput;
    private TextView             mResultOutput;
    private View                 mProgressBar;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(BroadcastHelper.ACTION_SUCCESS)) {
                if (!intent.hasExtra(BroadcastHelper.EXTRA_NUMBER) ||
                        !intent.hasExtra(BroadcastHelper.EXTRA_RESULT)) {
                    throw new IllegalStateException("Missed number and/or result.");
                }
                double number = intent.getDoubleExtra(BroadcastHelper.EXTRA_NUMBER, 0);
                double result = intent.getDoubleExtra(BroadcastHelper.EXTRA_RESULT, 0);
                if (number == Double.parseDouble(mNumberInput.getText().toString())) {
                    mResultOutput.setText(Double.toString(result));
                    mProgressBar.setVisibility(View.GONE);
                }
            } else if (intent.getAction().equals(BroadcastHelper.ACTION_ERROR)) {
                if (!intent.hasExtra(BroadcastHelper.EXTRA_NUMBER)) {
                    throw new IllegalStateException("Missed number.");
                }
                double number = intent.getDoubleExtra(BroadcastHelper.EXTRA_NUMBER, 0);
                if (number == Double.parseDouble(mNumberInput.getText().toString())) {
                    Toast.makeText(context, getString(R.string.error, number), Toast.LENGTH_LONG).show();
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNumberInput = (AutoCompleteTextView) findViewById(R.id.number_input);
        mResultOutput = (TextView) findViewById(R.id.result_output);
        mProgressBar = findViewById(R.id.progress);
        findViewById(R.id.calc_button).setOnClickListener(this);

        mNumberInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mResultOutput.setText("");
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharSequence(STATE_RESULT, mResultOutput.getText());
        outState.putInt(STATE_PROGRESS_VISIBILITY, mProgressBar.getVisibility());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(STATE_RESULT)) {
                mResultOutput.setText(savedInstanceState.getCharSequence(STATE_RESULT));
            }
            if (savedInstanceState.containsKey(STATE_PROGRESS_VISIBILITY)) {
                mProgressBar.setVisibility(savedInstanceState.getInt(STATE_PROGRESS_VISIBILITY));
            }
        }
    }

    @Override
    public void onClick(View v) {
        String num = mNumberInput.getText().toString();
        if (!TextUtils.isEmpty(num)) {
            mProgressBar.setVisibility(View.VISIBLE);
            CalcService.startActionDiv2(this, Double.parseDouble(num));
        } else {
            mNumberInput.setError(getString(R.string.input_number_hint));
        }
    }

    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BroadcastHelper.ACTION_SUCCESS);
        intentFilter.addAction(BroadcastHelper.ACTION_ERROR);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, intentFilter);
    }
}
