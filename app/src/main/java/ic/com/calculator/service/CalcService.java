package ic.com.calculator.service;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.j256.ormlite.android.apptools.OrmLiteBaseService;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import ic.com.calculator.api.Calculator;
import ic.com.calculator.api.DummyCounter;
import ic.com.calculator.api.model.MyNumberProto;
import ic.com.calculator.api.model.MyResponseProto;
import ic.com.calculator.database.DatabaseHelper;
import ic.com.calculator.database.NumberData;

/**
 * Service to send requests to server
 */
public class CalcService extends OrmLiteBaseService<DatabaseHelper> {

    private static final String ACTION_DIV_2 = "CalcService.ACTIONS.div_2";
    private static final String EXTRA_NUMBER = "CalcService.EXTRAS.number";

    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

    private static final int      KEEP_ALIVE_TIME      = 1;
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
    ThreadPoolExecutor mThreadPool;
    private final Object lock = new Object();
    private BlockingQueue<Runnable> mDecodeWorkQueue;

    private LinkedList<Double> mProcessingNumbers = new LinkedList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        mDecodeWorkQueue = new LinkedBlockingQueue<>();
        mThreadPool = new ThreadPoolExecutor(
                NUMBER_OF_CORES,       // Initial pool size
                NUMBER_OF_CORES,       // Max pool size
                KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT,
                mDecodeWorkQueue);
    }

    public static void startActionDiv2(Context context, double number) {
        Intent intent = new Intent(context, CalcService.class);
        intent.setAction(ACTION_DIV_2);
        intent.putExtra(EXTRA_NUMBER, number);
        context.startService(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        onHandleIntent(intent);
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_DIV_2.equals(action)) {
                double number = intent.getDoubleExtra(EXTRA_NUMBER, 0);
                mThreadPool.execute(new CalcRunnable(number));
            }
        }
    }

    private class CalcRunnable implements Runnable {
        private double mNumber;

        public CalcRunnable(double number) {
            mNumber = number;
        }

        @Override
        public void run() {
            RuntimeExceptionDao<NumberData, Integer> numberDao = getHelper().getNumberDataDao();
            List<NumberData> list = numberDao.queryForEq("number", mNumber);
            if (!list.isEmpty()) {
                BroadcastHelper.sendSuccessBroadcast(CalcService.this, list.get(0).getNumber(),
                        list.get(0).getProcessedvalue());
                return;
            }

            synchronized (lock) {
                if (mProcessingNumbers.contains(mNumber)) {
                    return;
                }
                mProcessingNumbers.add(mNumber);
            }

            MyNumberProto.MyNumber myNumber = MyNumberProto.MyNumber.newBuilder()
                    .setNumber(mNumber)
                    .build();
            Calculator calculator = new Calculator(new DummyCounter());
            MyResponseProto.MyResponse response = calculator.calculate(myNumber);
            if (response.getWithError()) {
                BroadcastHelper.sendErrorBroadcast(CalcService.this, response.getNumber());
            } else {
                BroadcastHelper.sendSuccessBroadcast(CalcService.this, response.getNumber(), response.getDivided());
                NumberData simple = new NumberData(response.getNumber(), response.getDivided());
                numberDao.create(simple);
            }

            synchronized (lock) {
                mProcessingNumbers.remove(mNumber);
            }
        }
    }
}
