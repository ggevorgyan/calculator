package ic.com.calculator.service;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

public class BroadcastHelper {

    public static final String ACTION_ERROR   = "BroadcastHelper.ACTIONS.error";
    public static final String ACTION_SUCCESS = "BroadcastHelper.ACTIONS.success";

    public static final String EXTRA_NUMBER = "BroadcastHelper.EXTRAS.number";
    public static final String EXTRA_RESULT = "BroadcastHelper.EXTRAS.result";

    public static void sendErrorBroadcast(Context context, double number) {
        Intent intent = new Intent();
        intent.setAction(ACTION_ERROR);
        intent.putExtra(EXTRA_NUMBER, number);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void sendSuccessBroadcast(Context context, double number, double reslt) {
        Intent intent = new Intent();
        intent.setAction(ACTION_SUCCESS);
        intent.putExtra(EXTRA_NUMBER, number);
        intent.putExtra(EXTRA_RESULT, reslt);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
